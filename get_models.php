<?php
session_start();
include 'bdd.php'; // Connexion à la base de données

header('Content-Type: application/json');

$userID = $_SESSION['userID'];

try {
    $stmt = $pdo->prepare("SELECT ID, NomProjet, created_at FROM projetcuisine WHERE UtilisateurID = ?");
    $stmt->execute([$userID]);
    $models = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($models, JSON_THROW_ON_ERROR);
    exit; // Ajoutez exit pour s'assurer qu'aucun autre caractère n'est envoyé
} catch (PDOException $e) {
    echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
    exit;
} catch (JsonException $je) {
    echo json_encode(['status' => 'error', 'message' => $je->getMessage()]);
    exit;
}