document.addEventListener('DOMContentLoaded', () => {
    // Ajouter la logique pour afficher/cacher les options de meubles
    document.getElementById('addMeubleBtn').addEventListener('click', () => {
        const meubleOptions = document.getElementById('meubleOptions');
        meubleOptions.classList.toggle('hidden');
    });

    // Charger les données des meubles depuis le serveur
    fetch('index.php')
        .then(response => response.json())
        .then(data => {
            console.log('Données reçues:', data); // Vérifier les données reçues
            const meubleOptions = document.getElementById('meubleOptions');
            data.forEach(meuble => {
                console.log('Ajouter meuble option:', meuble); // Vérifier chaque meuble
                const item = document.createElement('div');
                item.className = 'option-item';
                item.dataset.type = meuble.Nom;
                item.innerText = meuble.Nom.charAt(0).toUpperCase() + meuble.Nom.slice(1);
                item.addEventListener('click', () => {
                    addMeubleToToolbar(meuble.Nom, meuble);
                    meubleOptions.classList.add('hidden');
                });
                meubleOptions.appendChild(item);
            });
        })
        .catch(error => console.error('Erreur lors du chargement des meubles:', error));
});

function addMeubleToToolbar(type, meuble) {
    const toolbar = document.getElementById('toolbar');
    const item = document.createElement('div');
    item.className = 'toolbar-item';
    item.draggable = true;
    item.dataset.type = type;
    item.dataset.id = meuble.ID; // Utiliser l'ID de la base de données

    const img = document.createElement('img');
    img.src = meuble.Image;

    const deleteBtn = document.createElement('button');
    deleteBtn.className = 'delete-btn';
    deleteBtn.innerText = 'X';
    deleteBtn.addEventListener('click', (event) => {
        event.stopPropagation();
        console.log(`Suppression du meuble avec ID: ${meuble.ID}`); // Débogage
        removeMeuble(meuble.ID);
        item.remove();
        // Mettez à jour ici d'autres logiques comme le prix total, etc.
    });

    item.appendChild(img);
    item.appendChild(deleteBtn);
    item.addEventListener('dragstart', (event) => {
        event.dataTransfer.setData('type', JSON.stringify(meuble));
        event.dataTransfer.setData('id', meuble.ID);
    });

    toolbar.appendChild(item);
    console.log(`Meuble ajouté à la barre d'outils: `, meuble); // Débogage
}

function removeMeuble(id) {
    console.log(`Suppression des meubles dans le plan avec ID: ${id}`); // Débogage
    const container = document.getElementById('container');
    const meubles = container.querySelectorAll('.meuble');
    meubles.forEach(meuble => {
        console.log(`Vérification du meuble avec ID: ${meuble.dataset.id}`); // Débogage
        if (meuble.dataset.id === id) {
            console.log(`Meuble trouvé et supprimé : ${id}`); // Débogage
            meuble.remove();
        }
    });
}

function updateTotalPrice(amount) {
    const totalPriceElement = document.getElementById('totalPrice');
    let totalPrice = parseInt(totalPriceElement.innerText.replace(/[^0-9]/g, '')) || 0;
    totalPrice += amount;
    totalPriceElement.innerText = `Montant total: ${totalPrice}€`;
}

document.getElementById('container').addEventListener('dragover', (event) => {
    event.preventDefault();
});

document.getElementById('container').addEventListener('drop', (event) => {
    event.preventDefault();
    const meubleData = JSON.parse(event.dataTransfer.getData('type'));
    const uniqueId = event.dataTransfer.getData('id');
    console.log(`Ajout du meuble au plan avec ID: ${uniqueId}`); // Débogage
    const container = document.getElementById('container');
    const meuble = document.createElement('div');
    meuble.className = 'meuble';
    meuble.dataset.id = uniqueId; // Utiliser l'ID de la base de données
    meuble.style.left = `${event.clientX - container.getBoundingClientRect().left}px`;
    meuble.style.top = `${event.clientY - container.getBoundingClientRect().top}px`;
    meuble.style.width = `${meubleData.width}px`;
    meuble.style.height = `${meubleData.height}px`;
    meuble.style.backgroundImage = `url(${meubleData.plan})`; // Affiche le plan du meuble

    const rotateBtn = document.createElement('button');
    rotateBtn.className = 'rotate-btn';
    rotateBtn.innerText = '↻';
    rotateBtn.addEventListener('mousedown', startRotate);

    meuble.appendChild(rotateBtn);
    meuble.addEventListener('mousedown', startDrag);
    container.appendChild(meuble);
    console.log(`Meuble ajouté au plan: `, meuble); // Débogage
});

function startDrag(event) {
    const selectedMeuble = event.target.closest('.meuble');
    const offsetX = event.clientX - selectedMeuble.getBoundingClientRect().left;
    const offsetY = event.clientY - selectedMeuble.getBoundingClientRect().top;

    function onMouseMove(moveEvent) {
        const container = document.getElementById('container');
        const x = moveEvent.clientX - container.getBoundingClientRect().left - offsetX;
        const y = moveEvent.clientY - container.getBoundingClientRect().top - offsetY;
        selectedMeuble.style.left = `${x}px`;
        selectedMeuble.style.top = `${y}px`;
    }

    function onMouseUp() {
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onMouseUp);
    }

    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);
}

function startRotate(event) {
    const selectedMeuble = event.target.closest('.meuble');
    const initialMouseX = event.clientX;
    const initialMouseY = event.clientY;
    const initialRotation = getRotationDegrees(selectedMeuble);

    function onRotateMove(rotateEvent) {
        const dx = rotateEvent.clientX - initialMouseX;
        const dy = rotateEvent.clientY - initialMouseY;
        const angle = Math.atan2(dy, dx) * (180 / Math.PI);
        selectedMeuble.style.transform = `rotate(${initialRotation + angle}deg)`;
    }

    function onRotateUp() {
        document.removeEventListener('mousemove', onRotateMove);
        document.removeEventListener('mouseup', onRotateUp);
    }

    document.addEventListener('mousemove', onRotateMove);
    document.addEventListener('mouseup', onRotateUp);
}

function getRotationDegrees(element) {
    const st = window.getComputedStyle(element, null);
    const tr = st.getPropertyValue("transform");
    if (tr === "none" || !tr) {
        return 0;
    }
    const values = tr.split('(')[1].split(')')[0].split(',');
    const a = values[0];
    const b = values[1];
    const angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
    return angle < 0 ? angle + 360 : angle;
}