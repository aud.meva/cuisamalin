<?php
session_start();
include 'bdd.php';

if (isset($_POST['connecter'])) {
    $userID = $_POST['userID'];

    // Appel de la procédure stockée
    $sql = "CALL VerifierIDUtilisateur(?, @existe);";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(1, $userID, PDO::PARAM_INT);
    $stmt->execute();

    // Récupérer la valeur du paramètre OUT
    $sql = "SELECT @existe AS existe;";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($result && $result['existe']) {
        $_SESSION['userID'] = $userID;  // Stocker l'ID utilisateur dans la session
        header("Location: file.php");  // Rediriger vers la page principale
        exit;
    } else {
        echo "L'ID n'existe pas dans la base de données. Veuillez réessayer.";
    }
}
?>


