<?php
session_start();
$userID = isset($_SESSION['userID']) ? $_SESSION['userID'] : 'Non connecté';
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plan de Cuisine Simplifié</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="user-id">
        Utilisateur ID : <?php echo htmlspecialchars($_SESSION['userID']); ?>
    </div>
    <div class="toolbar" id="toolbar">
        <button id="addMeubleBtn">Ajouter un meuble ▼</button>
        <div id="meubleOptions" class="hidden">
            <!-- Les options de meubles seront ajoutées ici dynamiquement -->
        </div>
        <p id="totalPrice">Montant total: 0€</p>
    </div>
    <div class="container" id="container">
        <!-- Les meubles seront déposés ici -->
    </div>
    <div class="model-controls">
        <button id="saveModelBtn">Sauvegarder le modèle</button>
        <button id="createNewModelBtn">Créer un nouveau modèle</button>
        <button id="listModelsBtn">Lister les modèles</button>
    </div>
    <div id="modelList">
        <!-- La liste des modèles sera affichée ici -->
    </div>
    <script src="script.js"></script>
    <script src="model.js"></script>
</body>
</html>