let currentModelID = null; // Variable pour stocker l'ID du modèle actuel
let modelName = ""; // Variable pour stocker le nom du modèle

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveModelBtn').addEventListener('click', saveModel);
    document.getElementById('createNewModelBtn').addEventListener('click', createNewModel);
    document.getElementById('listModelsBtn').addEventListener('click', listModels);
});

function saveModel() {
    if (!currentModelID) {
        modelName = prompt("Entrez le nom du projet de cuisine:");
        if (!modelName) return;
    }

    const meubles = [];
    document.querySelectorAll('.meuble').forEach(meuble => {
        meubles.push({
            meubleID: meuble.dataset.id,
            positionX: parseInt(meuble.style.left, 10),
            positionY: parseInt(meuble.style.top, 10),
            largeur: meuble.offsetWidth,
            longueur: meuble.offsetHeight,
        });
    });

    const url = currentModelID ? `update_model.php?id=${currentModelID}` : 'save_model.php';
    console.log('Requête envoyée :', url);
    console.log('Données envoyées :', {
        nomProjet: modelName,
        meubles: JSON.stringify(meubles)
    });

    const formData = new FormData();
    formData.append('nomProjet', modelName);
    formData.append('meubles', JSON.stringify(meubles));

    fetch(url, {
        method: 'POST',
        body: formData
    })
    .then(response => {
        console.log('Statut de la réponse:', response.status);
        return response.text();  // Utilisez text() pour vérifier le contenu brut
    })
    .then(rawData => {
        console.log('Réponse brute du serveur:', rawData);
        let data;
        try {
            data = JSON.parse(rawData);
        } catch (error) {
            console.error('Erreur lors du parsing du JSON:', error);
            return;
        }
        console.log('Données JSON décodées:', data);
        if (data.status === 'success') {
            alert('Modèle de cuisine sauvegardé avec succès!');
            if (!currentModelID) {
                currentModelID = data.projetCuisineID;
            }
        } else {
            console.error('Erreur:', data.message);
        }
    })
    .catch(error => console.error('Erreur:', error));
}



function createNewModel() {
    currentModelID = null; // Réinitialiser l'ID du modèle actuel
    modelName = ""; // Réinitialiser le nom du modèle
    document.getElementById('container').innerHTML = ''; // Vider le conteneur
    document.getElementById('totalPrice').innerText = 'Montant total: 0€'; // Réinitialiser le montant total
}

function deleteModel(projetCuisineID) {
    fetch(`delete_model.php?id=${projetCuisineID}`)
        .then(response => response.json())
        .then(data => {
            if (data.status === 'success') {
                alert('Modèle supprimé avec succès!');
                listModels(); // Mettre à jour la liste des modèles
            } else {
                console.error('Erreur lors de la suppression du modèle:', data.message);
            }
        })
        .catch(error => console.error('Erreur lors de la suppression du modèle:', error));
}


function listModels() {
    fetch('get_models.php')
        .then(response => response.json())
        .then(models => {
            const modelList = document.getElementById('modelList');
            modelList.innerHTML = ''; // Vider la liste des modèles
            models.forEach(model => {
                const modelItem = document.createElement('div');
                modelItem.className = 'model-item';
                modelItem.innerHTML = `
                    <a href="#" data-id="${model.ID}">${model.NomProjet}</a>
                    <span>Ouvert le ${new Date(model.created_at).toLocaleDateString()}</span>
                    <button data-id="${model.ID}" class="delete-model">XX</button>
                `;
                modelList.appendChild(modelItem);

                modelItem.querySelector('a').addEventListener('click', (event) => {
                    event.preventDefault();
                    console.log('Chargement du modèle avec ID:', model.ID); // Débogage
                    loadModel(model.ID);
                });

                modelItem.querySelector('.delete-model').addEventListener('click', (event) => {
                    event.stopPropagation();
                    console.log('Suppression du modèle avec ID:', model.ID); // Débogage
                    deleteModel(model.ID);
                });
            });
        })
        .catch(error => console.error('Erreur lors de la récupération des modèles:', error));
}


document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('saveModelBtn').addEventListener('click', saveModel);
    document.getElementById('createNewModelBtn').addEventListener('click', createNewModel);
    document.getElementById('listModelsBtn').addEventListener('click', listModels);
});

function initDraggable(element) {
    element.addEventListener('mousedown', function(e) {
        const offsetX = e.clientX - parseInt(window.getComputedStyle(this).left);
        const offsetY = e.clientY - parseInt(window.getComputedStyle(this).top);

        const onMouseMove = (e) => {
            this.style.left = `${e.clientX - offsetX}px`;
            this.style.top = `${e.clientY - offsetY}px`;
        };

        document.addEventListener('mousemove', onMouseMove);

        document.addEventListener('mouseup', () => {
            document.removeEventListener('mousemove', onMouseMove);
        }, { once: true });
    });
}

function addRotateButton(meubleElement) {
    const rotateBtn = document.createElement('button');
    rotateBtn.innerHTML = '↻';
    rotateBtn.className = 'rotate-btn';
    rotateBtn.addEventListener('click', (e) => {
        e.stopPropagation();
        const currentRotation = meubleElement.dataset.rotation ? parseInt(meubleElement.dataset.rotation) : 0;
        const newRotation = (currentRotation + 90) % 360;
        meubleElement.style.transform = `rotate(${newRotation}deg)`;
        meubleElement.dataset.rotation = newRotation;
    });
    meubleElement.appendChild(rotateBtn);
}

function loadModel(projetCuisineID) {
    fetch(`load_model.php?id=${projetCuisineID}`)
        .then(response => response.json())
        .then(data => {
            console.log('Données du modèle chargées:', data); // Débogage
            if (data.status === 'error') {
                console.error('Erreur lors du chargement du modèle:', data.message);
                return;
            }
            createNewModel(); // Réinitialiser le plan
            currentModelID = projetCuisineID; // Définir l'ID du modèle actuel
            modelName = data.NomProjet; // Définir le nom du modèle

            data.meubles.forEach(item => {
                console.log('Chargement du meuble:', item); // Débogage
                const meubleElement = document.createElement('div');
                meubleElement.className = 'meuble';
                meubleElement.dataset.id = item.MeubleID;
                meubleElement.style.left = `${item.PositionX}px`;
                meubleElement.style.top = `${item.PositionY}px`;
                meubleElement.style.width = `${item.Largeur}px`;
                meubleElement.style.height = `${item.Longueur}px`;
                if (item.plan) {
                    meubleElement.style.backgroundImage = `url(${item.plan})`;
                } else {
                    console.warn('Le meuble n\'a pas de plan:', item); // Avertissement si le plan est manquant
                }
                document.getElementById('container').appendChild(meubleElement);
                initDraggable(meubleElement); // Ajouter la possibilité de déplacer
                addRotateButton(meubleElement); // Ajouter la possibilité de tourner
            });
        })
        .catch(error => console.error('Erreur lors du chargement du modèle:', error));
}
