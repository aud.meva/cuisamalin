<?php
header('Content-Type: application/json');
$user = 'rayane';
$password = ''; // Assurez-vous que le mot de passe est correct ou laissez vide si nécessaire

try {
    $pdo = new PDO('mysql:host=localhost;dbname=cuiamalin', $user, $password);
    $stmt = $pdo->query('SELECT * FROM meuble');
    $meubles = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($meubles);
    exit;
} catch (PDOException $e) {
    echo json_encode(['error' => $e->getMessage()]);
    exit;
}